#include <clocale>
#include <vk_terminal.hpp>

int main() {
  setlocale(LC_ALL, "");
  SizeComponent sizeComponent;
  TextBoxEntity textBoxEntity =
      TextBoxEntityBuilderSystem{}.position(1, 1).size(10,10).buffer(L"Русский язык это русский язык!").build();
  // textBoxEntity.getBasicBufferComponent().setBuffer(L"test");
  TextBoxEntityCalculateAreaTerminalSystem textBoxEntityCalculateAreaTerminalSystem;
  TextBoxEntityPrintSystem textBoxEntityPrintSystem;

  textBoxEntityCalculateAreaTerminalSystem.add(textBoxEntity);
  textBoxEntityCalculateAreaTerminalSystem.run();

  textBoxEntityPrintSystem.add(textBoxEntity);
  textBoxEntityPrintSystem.run(); 
   
}
