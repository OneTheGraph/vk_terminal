#include <vk_terminal.hpp>

void AreaComponent::setArea(uint32_t area) { _area = area; }

uint32_t AreaComponent::getArea() { return _area; }
