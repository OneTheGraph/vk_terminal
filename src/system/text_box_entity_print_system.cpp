// #include <iostream>
#include <vk_terminal.hpp>

/*
 * You can use UTF-8 Java/C++ style code like: L'\uE030' or use symbols: ₽*
 */

/*
 *
 * In progress:
 * [~]Add unicode string with A_BOLD, A_BLINK, etc.
    short colorpair;
    init_pair(1, COLOR_CYAN, COLOR_WHITE);
    wchar_t wstr[] = L"₽жив";
    wchar_t *russian_string = L"₽рубль";
    const wchar_t *russian_string_const = russian_string;
    cchar_t *abs = new cchar_t[6];
    cchar_t *russian = new cchar_t[wcslen(russian_string_const)];
    cchar_t ch_russian2[10];
    setcchar(abs, russian_string, A_BLINK, 1, NULL);
    wchar_t ch[] = {L'\uE030'};
    wchar_t ch1[] = L"\u20AC";
    wchar_t ch_russian[] = L"\u044F";
    wchar_t ch_russian1[] = L"₽ не умер!";
    wchar_t wstr123 = L'я';
    std::string plustring = "(☞ﾟヮﾟ)☞";
    add_wchstr(abs);
    mvaddwstr(2, 1, ch_russian);
    mvadd_wchnstr(1,1, russian, 3);
 *
 *
*/

void TextBoxEntityPrintSystem::add(TextBoxEntity& textBoxEntity) {
  _containerTextBoxEntity.push_back((TextBoxEntity*)(&textBoxEntity));
}

void TextBoxEntityPrintSystem::run() {
  uint16_t width, height, area, lengthOfBuffer, lengthOfBufferWithCursor,
      cursor, position_x, position_y;
  wchar_t* buffer;
  cchar_t* bufferPrint;

  for (TextBoxEntity* i : _containerTextBoxEntity) {
    position_y = i->getPositionPaddingComponent().getPositionWithPaddingY();
    position_x = i->getPositionPaddingComponent().getPositionWithPaddingX();

    cursor     = i->getBasicBufferComponent().getCursor();

    buffer     = i->getBasicBufferComponent().getBuffer();

    lengthOfBufferWithCursor =
        wcslen(i->getBasicBufferComponent().getBuffer()) -
        i->getBasicBufferComponent().getCursor();

    area   = i->getAreaPaddingComponent().getAreaWithPadding();
    height = i->getSizePaddingComponent().getHeightWithPadding();
    width  = i->getSizePaddingComponent().getWidthWithPadding();

    // lengthOfBuffer = wcslen(i->getBasicBufferComponent().getBuffer());
    if (lengthOfBufferWithCursor <= area) {
      lengthOfBuffer = lengthOfBufferWithCursor;
    } else {
      lengthOfBuffer = area;
    }
    bufferPrint = new cchar_t[lengthOfBuffer];
    for (uint16_t l = 0; l < lengthOfBuffer; l++) {
      bufferPrint[l] = {0, {buffer[l + cursor], L'\0'}};
    }
    uint16_t len = 0;
    for (uint16_t j = 0; j < height && len < lengthOfBuffer; j++) {
      for (uint16_t k = 0; k < width && len < lengthOfBuffer; k++) {
        // std::wcout << bufferPrint[cursor + l].chars[0];
        // mvwadd_wch(stdscr, 10, 12, &t);
        mvadd_wch(position_y + j, position_x + k, &bufferPrint[len]);
        len++;
      }
    }
    delete[] bufferPrint;
  }
}
