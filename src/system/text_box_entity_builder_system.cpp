#include <cstdlib>
#include <cstring>
#include <vk_terminal.hpp>
/*
TextBoxEntityBuilderSystem::TextBoxEntityBuilderSystem() {
  _sizeComponent = SizeComponent;
  _areaComponent = AreaComponent;
  _positionComponent = new PositionComponent;
  _bufferComponent = new BufferComponent;
  _borderComponent = new BorderComponent;
}
*/

TextBoxEntityBuilderSystem& TextBoxEntityBuilderSystem::size(uint16_t height,
                                                             uint16_t width) {
  _sizeComponent.setHeight(height);
  _sizeComponent.setWidth(width);
  uint32_t _area = height * width;
  _areaComponent.setArea(_area);
  return *this;
}

TextBoxEntityBuilderSystem& TextBoxEntityBuilderSystem::position(uint16_t y,
                                                                 uint16_t x) {
  _positionComponent.setPositionY(y);
  _positionComponent.setPositionX(x);
  return *this;
}
/*
 * TODO: Release this
 * */
TextBoxEntityBuilderSystem& TextBoxEntityBuilderSystem::setBorder(bool border) {
  _borderComponent.setHaveBorder(border);
  // _borderComponent.setBorder();
  return *this;
}

TextBoxEntityBuilderSystem& TextBoxEntityBuilderSystem::buffer(
    wchar_t* buffer) {
  _basicBufferComponent.setCursor(0);
  _basicBufferComponent.setBuffer(buffer);
  return *this;
}

TextBoxEntity TextBoxEntityBuilderSystem::build() {
  uint16_t heightWithPadding = _sizeComponent.getHeight();
  uint16_t widthWithPadding = _sizeComponent.getWidth();
  uint16_t areaWithPadding;

  //  if (_borderComponent.getHaveBorder()) {
  //    _sizePaddingComponent.setPadding(1);
  //    heightWithPadding -= 2;
  //    widthWithPadding  -= 2;
  //    _sizePaddingComponent.setHeightWithPadding(heightWithPadding);
  //    _sizePaddingComponent.setHeightWithPadding(widthWithPadding);
  //    areaWithPadding = heightWithPadding * widthWithPadding;
  //    _areaPaddingComponent.setAreaWithPadding(areaWithPadding);
  //  } else {
  //    areaWithPadding = heightWithPadding * widthWithPadding;
  //    _areaPaddingComponent.setAreaWithPadding(areaWithPadding);
  //  }

  // Default constructor
  return TextBoxEntity(_sizeComponent, _areaComponent, _positionComponent,
                       _basicBufferComponent, _borderComponent,
                       _sizePaddingComponent, _areaPaddingComponent,
                       _positionPaddingComponent);
}

/*TextBoxEntityBuilderSystem::TextBoxEntityBuilderSystem() {
  delete [] _sizeComponent;
  delete [] _areaComponent;
  delete [] _positionComponent;
  delete [] _positionComponent;
  delete [] _bufferComponent;
  delete [] _borderComponent;
}*/
