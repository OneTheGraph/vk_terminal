#include <vk_terminal.hpp>

void TextBoxEntityCalculateWithPaddingTerminalSystem::add(
    TextBoxEntity& textBoxEntity) {
  _containerTextBoxEntity.push_back((TextBoxEntity*)(&textBoxEntity));
}

// If have border -> refresh SizePaddingComponent, AreaPaddingComponent and
// PositionPaddingComponent
void TextBoxEntityCalculateWithPaddingTerminalSystem::run() {
  uint16_t heightWithPadding, widthWithPadding, areaWithPadding,
      positionWithPadding_y, positionWithPadding_x;
  for (TextBoxEntity* i : _containerTextBoxEntity) {
    if (i->getBorderComponent().getHaveBorder()) {
      heightWithPadding = i->getSizeComponent().getHeight() - 2;
      widthWithPadding = i->getSizeComponent().getWidth() - 2;
      areaWithPadding = widthWithPadding * heightWithPadding;
      positionWithPadding_y = i->getPositionComponent().getPositionY() + 1;
      positionWithPadding_x = i->getPositionComponent().getPositionX() + 1;
    } else {
      heightWithPadding = i->getSizeComponent().getHeight();
      widthWithPadding = i->getSizeComponent().getWidth();
      areaWithPadding = widthWithPadding * heightWithPadding;
      positionWithPadding_y =
          i->getPositionPaddingComponent().getPositionWithPaddingY();
      positionWithPadding_x =
          i->getPositionPaddingComponent().getPositionWithPaddingX();
    }
    // SizePaddingComponent sizePaddingComponent{};
    i->getSizePaddingComponent().setHeightWithPadding(heightWithPadding);
    i->getSizePaddingComponent().setWidthWithPadding(widthWithPadding);

    i->getAreaPaddingComponent().setAreaWithPadding(areaWithPadding);

    i->getPositionPaddingComponent().setPositionWithPaddingY(
        positionWithPadding_y);
    i->getPositionPaddingComponent().setPositionWithPaddingX(
        positionWithPadding_x);
  }
  _containerTextBoxEntity.clear();
}
