#include <vk_terminal.hpp>

void TerminalEntity::setSizeComponent(SizeComponent sizeComponent) {
  _sizeComponent = sizeComponent;
}
void TerminalEntity::setAreaComponent(AreaComponent areaComponent) {
  _areaComponent = areaComponent;
}
void TerminalEntity::setTerminalColorComponent(
    TerminalColorComponent terminalColorComponent) {
  _terminalColorComponent = terminalColorComponent;
}

SizeComponent TerminalEntity::getSizeComponent() { return _sizeComponent; }
AreaComponent TerminalEntity::getAreaComponent() { return _areaComponent; }
TerminalColorComponent TerminalEntity::getTerminalColorComponent() {
  return _terminalColorComponent;
}

TerminalEntity::TerminalEntity(SizeComponent sizeComponent,
                               AreaComponent areaComponent,
                               TerminalColorComponent terminalColorComponent) {
  _sizeComponent = sizeComponent;
  _areaComponent = areaComponent;
  _terminalColorComponent = terminalColorComponent;
}
