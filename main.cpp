//
// Created by main on 27.01.2020.
//

#include <any>
#include <clocale>
#include <vk_terminal.hpp>

/*
 *
 * Add setlocale() to Windows macros
 *
 * void getmaxyx(WINDOW *win, int y, int x);
 */

// void resizehandler(int);

void terminal_start();

void terminal_stop();

// sig_atomic_t signaled = 0;

int main() {
  // Locale(Cyrillic doesn't work without this)
  setlocale(LC_ALL, "");
  // Start terminal: run all important settings
  terminal_start();
  // SizeComponent sizeComponent;
  TerminalEntity terminalEntity = TerminalEntityBuilderSystem{}.build();
  TextBoxEntity textBoxEntity1 = TextBoxEntityBuilderSystem{}
                                    .position(1, 1)
                                    .size(10, 10)
                                     .setBorder(true)
                                    .buffer(L"₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽₽g")
                                    .build();
  TextBoxEntity textBoxEntity2 = TextBoxEntityBuilderSystem{}
                                    .position(20, 20)
                                    .size(4, 20)
                                     .setBorder(true)
                                    .buffer(terminalEntity.getTerminalColorComponent().getTerminalEnvVariable())
                                    .build();
  // textBoxEntity.getBasicBufferComponent().setBuffer(L"test");
  TextBoxEntityCalculateWithPaddingTerminalSystem
      textBoxEntityCalculateAreaWithPaddingTerminalSystem;
  TextBoxEntityPrintSystem textBoxEntityPrintSystem;

  textBoxEntityCalculateAreaWithPaddingTerminalSystem.add(textBoxEntity1);
  textBoxEntityCalculateAreaWithPaddingTerminalSystem.add(textBoxEntity2);
  textBoxEntityCalculateAreaWithPaddingTerminalSystem.run();

  textBoxEntityPrintSystem.add(textBoxEntity1);
  textBoxEntityPrintSystem.add(textBoxEntity2);
  textBoxEntityPrintSystem.run();
  refresh();
  // Create size and signal to respond resize terminal
  // Set test buffer and show text
  // \uE030\uE029";
  // textbox1.set_buffer(string);
  // textBox2.set_buffer(string);

  // void (*funcptr)(int);
  // Handle sig and function to respond resize terminal
  // signal(SIGWINCH, resizehandler);
  // funcptr = signal(SIGWINCH, resizehandler);

  // Code that should be distributed by the classes
  // std::queue<WINDOW *> windows;
  // windows.push();
  // std::map<int, std::function<void()>> keylist;

  // int key = 0;
  // Do reworking exit on cycles
  /*while (key != 27) {
    textBoxViewer.show(textbox1, size);
    // c = 0;
    key = getch();
    keyController.control(key);

    if (key == 's') {
      textbox1.set_buffer(L"fjdsafhdasjk");
      textBoxViewer.show(textBox2, size);
    }
    if (key == 'd') {
      textbox1.set_buffer(L"text");
    }
    if (key == KEY_UP) {
      textbox1.set_buffer(L"Key up");
    }
    if (key == 0x9) {
      textbox1.set_buffer(L"Tab key");
    }

    // textbox1.set_buffer(L"Проверка!");
  }*/
  //    while (true) {
  //        clear();
  //        if (1 == signaled) {
  //            size->getSize();
  //            textbox1.set_height(size->height() / 2);
  //            textbox1.set_width(size->width() / 2);
  //            signaled = 0;
  //        }
  //        textBoxViewer.show(textbox1, size, parameter);
  //
  //        // handle input
  //        c = 0;
  //        c = getch();
  //
  //        if (c == 'q' || c == 27 /*ESC*/)
  //            break;
  //    }
  // sleep(5);
  getch();
  terminal_stop();
  return 0;
}

void terminal_start() {
  // Initialization
  initscr();
  // Turn on Ctrl+C
  cbreak();
  // raw();
  // Start color
  start_color();
  // Turn off cursor
  curs_set(0);
  // Turn off writing keyboard symbols
  noecho();
  // Turn on keypad keys(KEY_UP, KEY_DOWN, etc.)
  keypad(stdscr, TRUE);
  // meta(stdscr, TRUE);
  // Refresh. Without this doesn't work
  refresh();
  // Timeout for non delay getch();
  timeout(-1);
}

void terminal_stop() {
  // End terminal work
  endwin();
}

/*void resizehandler(int) {
  // Stop terminal
  terminal_stop();
  // Restart terminal
  terminal_start();
  signaled = 1;
  // Get size
}*/
