//
// Created by main on 12.01.2021.
//

#ifndef VK_TERMINAL_COMPONENTS_HPP
#define VK_TERMINAL_COMPONENTS_HPP

class Size {
 private:
  uint16_t _height;
  uint16_t _width;
  std::vector<uint16_t>* _dimension;

 public:
  void setHeight(uint16_t height);
  void setWidth(uint16_t width);

  uint16_t getHeight();
  uint16_t getWidth();
};

class Area {
 private:
  uint32_t _area;

 public:
  void setArea(uint32_t area);

  uint32_t getArea();
};

class Position {
 private:
  uint16_t _x;
  uint16_t _y;
  std::vector<uint16_t>* _dimension;

 public:
  void setX(uint16_t x);
  void setY(uint16_t y);

  uint16_t getX();
  uint16_t getY();
};

class TerminalBorder {
 private:
  bool _have;
  // ls, rs, ts, bs, tl, tr, bl, br
  std::vector<cchar_t> _symbols;
  wchar_t* _name;

 public:
  void setHaveBorder(bool haveBorder);
  // void setBorder();

  bool getHaveBorder();

  wchar_t* getNameBorder();
};

class PaddingSize {
 private:
  uint16_t _height;
  uint16_t _width;
  std::vector<uint16_t>* _dimension;

 public:
  void setHeight(uint16_t height);
  void setWidth(uint16_t width);

  uint16_t getHeight();
  uint16_t getWidth();
};

class PaddingArea {
 private:
  uint32_t _area;

 public:
  void setArea(uint32_t area);

  uint32_t getArea();
};

class PaddingPosition {
 private:
  uint16_t _x;
  uint16_t _y;
  std::vector<uint16_t>* _dimension;

 public:
  void setX(uint16_t x);
  void setY(uint16_t y);

  uint16_t getX();
  uint16_t getY();
};

using entityID = uint16_t;

template <typename Type>
using ComponentMap = std::unordered_map<entityID, Type>;

using Sizes = ComponentMap<Size&>;
using Areas = ComponentMap<Area&;
using Positions = ComponentMap<Position&>;
using TerminalBorders = ComponentMap<TerminalBorder&>;
using PaddingSizes = ComponenMap<PaddingSize&>;
using PaddingAreas = ComponenMap<PaddingArea&>;
using PaddingPositions = ComponentMan<PaddingPosition&>;

#endif  // VK_TERMINAL_COMPONENTS_HPP
